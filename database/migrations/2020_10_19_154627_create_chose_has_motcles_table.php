<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChoseHasMotclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('chose_has_motcles', function (Blueprint $table) {
            $table->id();
            $table->foreignId('chose_ch_id')->constrained('choses');
            $table->foreignId('motcle_mot_id')->constrained('motcles');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chose_has_motcles');
    }
}
