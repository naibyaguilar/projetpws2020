<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChosesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('choses', function (Blueprint $table) {
            $table->id();
            $table->foreignId('ch_li_id')->constrained('lieus');
            $table->foreignId('ch_sous_id')->constrained('sous_categories');
            $table->string('ch_nom');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('choses');
    }
}
