<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chose;
use App\Lieu;
use App\Categorie;
use App\Motcle;
use App\SousCategorie;
use App\ChoseHasMotcle;

class ChosesController extends Controller
{
    public function index()
    {
        $choses = Chose::all();
        $cat = Categorie::All();
        $sous = SousCategorie::All();
        $mot = ChoseHasMotcle::All();

        $message = "";
        if ($choses->count() < 1) {
            $message = "Y'a pas des choses dispo";
        }
        return view('choses.index', compact('choses', 'cat', 'sous', 'message','mot'));
    }

    public function create()
    {
        $lieux = Lieu::all();
        $categories = Categorie::all();
        $sous = SousCategorie::all();

        $chose = new Chose();
        $mot = Motcle::all();

        return view('choses.create', compact('lieux', 'categories', 'sous', 'chose', 'mot'));
    }

    public function store()
    {

        request()->validate([
            'ch_nom' => 'required|min:3',
            'ch_li_id' => 'required|integer',
            'ch_sous_id' => 'required|integer',
            'cat' => 'required|integer',
            'check_mot_cles' => 'required'
        ]);

        $ch_nom = request('ch_nom');
        $ch_li_id = request('ch_li_id');
        $ch_sous_id = request('ch_sous_id');
        $check_box = request("check_mot_cles");

        $id_chose = Chose::create([
            'ch_li_id' => $ch_li_id,
            'ch_sous_id' => $ch_sous_id,
            'ch_nom' => $ch_nom
        ])->id;


        foreach ($check_box as $value) {
            $last_id = ChoseHasMotcle::create([
                'chose_ch_id' => $id_chose,
                'motcle_mot_id' => $value
            ])->id;
        }
        return redirect('/choses');
    }



    public function getSous(Request $request)
    {
        if ($request->ajax()) {
            $sous = SousCategorie::where('sous_cat_id', $request->cat_id)->get();
            foreach ($sous as $souscat) {
                $sousArray[$souscat->id] = $souscat->sous_nom;
            }
            return response()->json($sousArray);
        }
    }

    public function edit(Chose $chose)
    {
        $lieux = Lieu::all();
        $categories = Categorie::all();
        $sous = SousCategorie::all();

        return view('choses.edit', compact('lieux', 'chose', 'categories', 'sous'));
    }

    public function update(Chose $chose)
    {
        $data = request()->validate([
            'ch_nom' => 'required|min:3',
            'ch_li_id' => 'required|integer',
            'ch_sous_id' => 'required|integer',
        ]);

        $chose->update($data);

        return redirect('/choses');
    }

    public function destroy(Chose $chose)
    {
        $chose->delete();

        return redirect('/choses');
    }

    public function rechercheChose()
    {
        $search = request()->validate([
            'nom' => 'required:min:3'
        ]);
        $message = "";
        if ($search) {

            $choses = Chose::whereHas('lieus', function ($q) use ($search) {
                $q->where('li_nom', 'LIKE', "%" . $search["nom"] . "%");
            })->get();

            if ($choses->count() < 1) {
                $choses = Chose::where('ch_nom', 'LIKE', "%" . $search["nom"] . "%")->get();
            }
            if ($choses->count() < 1) {
                $message = "y'a pas des choses pour cette recherche";
            }
            $cat = Categorie::All();
            $sous = SousCategorie::All();
            return view('choses.index', compact('choses', 'cat', 'sous', 'message'));
        }
    }
}
