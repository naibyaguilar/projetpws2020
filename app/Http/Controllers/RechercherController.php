<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chose;
use App\Lieu;

class RechercherController extends Controller
{
    //

    public function index(){
        $choses="";
        $message = "Y'a pas de rechercher des trucs encore";
        return view('rechercher.index', compact('choses', 'message'));
    }

    public function recherche(){
        $search = request()->validate([
            'nom'=> 'required:min:3'
        ]);
        $message="";
        if($search){
            $choses= Chose::whereHas('lieus', function($q) use ($search) {
                $q->where('li_nom', 'LIKE', "%". $search["nom"]."%");
            })->get();
            if($choses->count()<1){
                $choses = Chose::where('ch_nom', 'LIKE', "%". $search["nom"]. "%")->get();
            }
            if($choses->count() <1 ){
                $message = "y'a pas des trucs pour cette recherche";
            }
             /*$cat = Categorie::All();
              $sous = SousCategorie::All();*/
            return view('rechercher.index', compact('choses','cat','sous', 'message'));
        }
    }


}

