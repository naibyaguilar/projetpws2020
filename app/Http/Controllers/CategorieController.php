<?php

namespace App\Http\Controllers;

use App\Categorie;
use App\SousCategorie;
use App\Chose;
use Illuminate\Http\Request;

class CategorieController extends Controller
{
    public function list(){
        $categories = Categorie::all();
        $sous = SousCategorie::all();
      return view('categorie.index', compact('categories', 'sous'));
    }

    public function create(){
        return view('categorie.create', ["categorie" => new Categorie]);
    }
    public function edit(Categorie $categorie){
        $sous = SousCategorie::all();
        return view('categorie.edit', compact('categorie', 'sous'),["categorie" => new Categorie]);
    }
    public function store(){
        $data = request()->validate([
            'cat_nom'=>'required|min:3'
        ]);
        Categorie::create($data);
        return redirect()->route('categorie.index');
    }

    public function update(Categorie $Categorie){
        $data = request()->validate([
            'cat_nom' => 'required|min:3'
        ]);
        $Categorie->update($data);

        return redirect()->route('categorie.index');
    }

    public function destroy(Categorie $categorie){
        if (Categorie::find($categorie->id)){
        }else{
            $categorie->delete();

         }
        return redirect()->route('categorie.index');
    }

    public function rechercheCategorie(){
        $nom = request()->validate([
            'nom'=>'required|min:3'
        ]);
        if($nom){
            $categories = Categorie::OrderBy('id', 'DESC')->where('cat_nom', 'LIKE', "%$nom%");
            return view('categorie.show', compact('categories'));
        }
    }

    public function show(Categorie $categorie){
        $sous = SousCategorie::where('sous_cat_id', '=', $categorie->id)->get();
        $choses = Chose::all();

        //dd($choses);
        return view('categorie.show', compact('categorie', 'sous', 'choses'));
    }

}
