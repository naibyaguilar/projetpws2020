<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SousCategorie;
use App\Categorie;
use App\Chose;

class SousCategorieController extends Controller
{
    //
    public function create(Categorie $categorie){
        //$categorie = Categorie::all();
        $SousCategorie = new SousCategorie();
        return view('categorie.souscategorie.create',compact('SousCategorie', 'categorie'));
    }
    public function edit(string $id){
        $sousCategorie = SousCategorie::find($id);

        return view('categorie.souscategorie.edit', compact('sousCategorie'));
        /* , compact('sousCategorie'), ["souscategories" => new SousCategorie] */
    }

    public function store(Categorie $categorie){
        $data = request()->validate([
            'sous_nom'=>'required|min:3',
            'sous_cat_id'=>'required|integer'
        ]);
        SousCategorie::create($data);
        return redirect('/cat');
    }

    public function update(SousCategorie $souscategorie){
        $data = request()->validate([
            'sous_nom' => 'required|min:3'
        ]);
        $souscategorie->update($data);

        return redirect()->route('categorie.index');
    }

    public function destroy(string $id){

        if (Chose::find($id)){

        }else{
            $sousCategorie = SousCategorie::find($id);

            $sousCategorie->delete();

         }
        return redirect()->route('categorie.index');
    }

    public function show(SousCategorie $souscategorie){
        $choses = Chose::all();

        //dd($choses);
        return view('categorie.souscategorie.show', compact('souscategorie', 'choses'));
    }

}
