<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lieu;
use Illuminate\Support\Facades\DB;

class LieuController extends Controller
{
    public function index(){
        $lieux = Lieu::all();
        return view('lieux.index', compact('lieux'));
    }
    public function create(){
        $lieux = Lieu::all();
        $lieu = new Lieu();

        return view('lieux.create', compact('lieux', 'lieu'));
    }
    public function store(){
        $data = request()->validate([
            'li_nom' => 'required|min:3',
            'li_dans_id' => 'required|integer'
        ]);

        Lieu::create($data);

        return redirect('/lieu');
    }
    public function edit(Lieu $lieu){
        $lieux = Lieu::all();
        return view('lieux.edit', compact('lieu', 'lieux'));
    }
    public function update(Lieu $lieu){
        $data = request()->validate([
            'li_nom' => 'required|min:3',
            'li_dans_id' => 'required|integer'
        ]);

        $lieu->update($data);

        return redirect('/lieu');
    }

    public function destroy(Lieu $lieu){
        $lieu->delete();

        return redirect('lieu');
    }

    public function search(){
        $search_text = $_GET['query'];
        $lieux = Lieu::where('li_nom', 'LIKE', '%'.$search_text.'%')->get();

        return view('lieux.search', compact('lieux'));
    }
}
