<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Motcle extends Model
{
    protected $guarded = [];

    public function choseHasMotcle(){
        return $this->hasMany('App\ChoseHasMotcle');
    }
}
