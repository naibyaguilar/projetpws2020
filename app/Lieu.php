<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lieu extends Model
{
    protected $guarded = [];

    public function children(){
        return $this->hasMany('App\Lieu', 'li_dans_id', 'id');
    }
    public function parent(){
        return $this->hasOne('App\Lieu', 'id', 'li_dans_id');
    }

    public function choses(){
        return $this->hasMany('App\Chose', 'id');
    }
}
