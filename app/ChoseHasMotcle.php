<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChoseHasMotcle extends Model
{
    protected $guarded = [];

    public function chose(){
        return $this->belongsTo('App\Chose', 'chose_ch_id');
    }

    public function motcle(){
        return $this->belongsTo('App\Motcle', 'motcle_mot_id');
    }
}
