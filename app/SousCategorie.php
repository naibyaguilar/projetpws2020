<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SousCategorie extends Model
{
    protected $guarded =[];

    public function categorie(){
        return $this->belongsTo('App\Categorie', 'sous_cat_id');
    }

    public function chose(){
        return $this->hasMany('App\Chose', 'id');
    }
}
