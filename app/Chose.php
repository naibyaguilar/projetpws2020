<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chose extends Model
{
    protected $guarded = [];

    public function lieus(){
        return $this->belongsTo('App\Lieu', 'ch_li_id');
    }

    public function sousCategorie(){
        return $this->belongsTo('App\SousCategorie', 'ch_sous_id');
    }

    public function choseHasMotcle(){
        return $this->hasMany('App\ChoseHasMotcle');
    }
}
