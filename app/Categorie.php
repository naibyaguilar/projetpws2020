<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{

    protected $guarded =[];

    public function sousCategorie(){
        return $this->hasMany('App\SousCategorie', 'id');
    }

}
