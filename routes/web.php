<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('home');
});

// choses
Route::get('/choses', 'ChosesController@index');

Route::get('/choses/create','ChosesController@create');

Route::post('choses', 'ChosesController@store');

Route::post('choses/recherche', 'ChosesController@rechercheChose');

Route::get('/sous-categories', 'ChosesController@getSous');

Route::get('choses/{chose}/edit', 'ChosesController@edit');

Route::patch('choses/{chose}', 'ChosesController@update');

Route::delete('choses/{chose}', 'ChosesController@destroy');

// lieux
/*
Route::get('/lieu','LieuController@index');

Route::get('/lieu/create','LieuController@create');

Route::post('lieu', 'LieuController@store');

Route::get('lieu/{lieu}/edit', 'LieuController@edit');

Route::patch('lieu/{lieu}', 'LieuController@update');

Route::delete('lieu/{lieu}', 'LieuController@destroy');
*/

Route::resource('lieu', 'LieuController');
Route::get('/search', 'LieuController@search');

//categorie


Route::get('cat', 'CategorieController@list')->name("categorie.index");

Route::get('/sous', 'CategorieController@Sous');

Route::get('/categorie/{categorie}/edit', 'CategorieController@edit');
Route::get('/categorie/{categorie}/show', 'CategorieController@show');

Route::post('/categorie/recherche', 'CategorieController@rechercheCategorie');

Route::resource('categorie', 'CategorieController')->names('categories');
/*
Route::post('/categorie/store', 'CategorieController@store');
*/

//Souscategorie

Route::get('/souscategorie/{categorie}/create', 'SousCategorieController@create');

Route::post('/souscategorie/{categorie}', 'SousCategorieController@store');

Route::get('/souscategorie/{id}/edit', 'SousCategorieController@edit');
Route::get('/souscategorie/{souscategorie}/show', 'SousCategorieController@show');

Route::patch('/souscategorie/{souscategorie}', 'SousCategorieController@update');

Route::delete('/souscategorie/{id}/destroy', 'SousCategorieController@destroy');

Route::resource('souscategorie', 'SousCategorieController')->names('souscategories');

//Rechercher
Route::get('/rechercher', 'RechercherController@index');
Route::post('/recherche', 'RechercherController@recherche');



