@extends('layout.layout')

@section('content')
<div class="widget-wrap">
<h1 style="color:black;">Modification du chose << {{$chose->ch_nom}} >></h1>
<form action="/choses/{{$chose->id}}" method="POST">
    @method('PATCH')
    @include('forms.form-chose-edit')
    <button type="submit" class="btn btn-primary">Sauvergarder les informations</button>
</form>
</div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $('#select2').on('change', function(){
                var cat_id = $(this).val();
                if($.trim(cat_id) != ''){
                    $.get('/sous-categories', {cat_id: cat_id}, function(categories){
                        $('#select3').empty();
                        $('#select3').append("<option value=''>Sélectionner...</option>");
                        $.each(categories, function(index, value){
                            $('#select3').append("<option value='"+index+"'>"+value+"</option>");
                        })
                    });
                }
            });
        });
    </script>
@endsection
