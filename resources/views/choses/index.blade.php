@extends('home')
<!-- Chose  -->
@section('content')
<div class="page-section">
      <div class="container">
        <div class="widget-wrap">

            <a class="btn btn-sm btn-primary" style="float: right;" href="/choses/create">Ajouter de Chose</a>
            <h3 class="widget-title" style="text-align: center; color: black;">CHOSES</h3>
            <br/>
            <div class="card-page">
              @if($message !="")
              <div class="alert alert-info row justify-content-center" role="alert">
                {{-- No hay resultados para la busqueda --}}
                {{ $message }}
              </div>
              @endif
              <div class="row row-beam-md">

                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Nom</th>
                      <th scope="col">Lieu</th>
                      <th scope="col">Catégorie</th>
                      <th scope="col">Sous-catégorie</th>
                      <th scope="col">Mots-clés</th>
                      <th scope="col">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($choses as $item)
                    <tr>
                      <th scope="row">{{$item->id}}</th>
                      <td>{{$item->ch_nom}}</td>
                      <td>{{$item->lieus->li_nom}}</td>
                      <td>{{$item->sousCategorie->categorie->cat_nom}}</td>
                      <td>{{$item->sousCategorie->sous_nom}}</td>
                      <td>
                          @foreach ($mot as $mots)
                          @if ($mots->chose->id == $item->id)
                        <div class="tag-clouds">
                            <a class="tag-cloud-link">{{ $mots->motcle->mot_nom}}</a>
                        </div>
                        @endif
                          @endforeach

                      </td>
                      <td>
                        <a href="/choses/{{$item->id}}/edit" class="btn btn-info">Modifier</a>
                        <form action="/choses/{{$item->id}}" method="POST" style="display: inline">
                          @csrf
                          @method('DELETE')
                          <button type="submit" class="btn btn-danger">Supprimer</button>
                        </form>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>

            </div>
        </div>
    </div>
</div>


@endsection
