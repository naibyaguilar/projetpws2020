@extends('home')
<!-- Chose  -->
@section('content')
<a class="btn btn-sm btn-primary" style="float: right;" href="/choses/create">Ajouter de Chose</a>
<br/>
<div class="container" >

      <!--asi? -->
    <h3>Catégories</h3>
    <ul class="nav nav-tabs" id="categorieTab" role="tablist">
        @foreach ($categories as $categorie)
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="{{$categorie->id}}" data-toggle="tab" href="#Categorie" role="tab" aria-controls="profile"
                    aria-selected="true">{{ $categorie->cat_nom }}</a>
            </li>
         @endforeach
    </ul>

    <br>
    <div class="tab-content" id="myTabContent">
        <div class="tab-content" id="SousCategorie">
            <ul class="nav nav-tabs" id="SousCategorieTab" role="tablist">
                @foreach ($sous as $souscategorie)
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="{{$souscategorie->id}}" data-toggle="tab" href="#Sous-Categorie" role="tab" aria-controls="home"
                            aria-selected="true">{{ $souscategorie->sous_nom }}</a>
                    </li>
                @endforeach
            </ul>
        </div>

    </div>

</div>
