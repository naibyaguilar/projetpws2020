@extends('home')
<!-- categorie  -->
@section('content')
<div class="widget-wrap">
<h3 style="text-align: center; color: black;">Enregistrer une Categorie</h3>
    <form  method="POST" action="{{ route('categories.store')}}">
        @include('forms.form-categorie')
        <button type="submit" class="btn btn-primary">Enregistrer</button>
    </form>
</div>
@endsection
