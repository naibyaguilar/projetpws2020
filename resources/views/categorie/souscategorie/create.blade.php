@extends('home')
<!-- categorie  -->
@section('content')
<div class="widget-wrap">
<h3 style="text-align: center; color: black;">Enregistrer un Sous-Categorie de <p> << {{ $categorie->cat_nom }}  >></p></h3>
    <form method="POST" action="/souscategorie/{{$categorie->id}}">
        @include('forms.form-souscategorie')
        <button type="submit" class="btn btn-primary">Enregistrer</button>
    </form>
</div>
@endsection

