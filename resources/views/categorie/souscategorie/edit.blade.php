@extends('home')
<!-- categorie  -->
@section('content')
<div class="widget-wrap">
    <h1 style="text-align: center; color: black;">MODIFICATION DE SOUS-CATEGOIRE </h1>
    <form action="/souscategorie/{{ $sousCategorie->id }}"  method="POST">
        @method('PATCH')
        @include('forms.form-sous-edit')
        <button type="submit" class="btn btn-primary">Sauvergarder les informations</button>
    </form>
</div>
@endsection
