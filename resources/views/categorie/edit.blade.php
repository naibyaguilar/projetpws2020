@extends('home')
<!-- categorie  -->
@section('content')
<div class="widget-wrap">
<h1 style="text-align: center; color: black;">MODIFICATION DE CATEGOIRE <p><< {{$categorie->cat_nom}} >></p></h1>
<form class="float-right" method="POST" action="{{ route('categories.destroy', $categorie) }}">
	@csrf @method("DELETE")
	<button type="submit" class="btn btn-danger">Supprimir</button>
</form>
<form action="{{route('categories.update',$categorie)}}" method="POST">
    @method('PATCH')
    @include('forms.form-categorie')
    <button type="submit" class="btn btn-primary">Sauvergarder les informations</button>
  </form>
  <br/>
  <hr>
<div>
    <table class="table">
        <h3>Sous Categorie</h3>
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nom</th>
            <th scope="col">...</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($sous as $sousCategorie)
          @if ($sousCategorie->sous_cat_id == $categorie->id)
          <tr></tr>
            <th scope="row">{{$sousCategorie->id}}</th>
            <td>{{$sousCategorie->sous_nom}}</td>
            <td>
            <a href="/souscategorie/{{$sousCategorie->id}}/edit" class="btn btn-primary" >Modifier</a>
              <form action="/souscategorie/{{$sousCategorie->id}}/destroy" method="POST" style="display: inline">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Supprimer</button>
              </form>
            </td>
          </tr>
          @endif
          @endforeach
        </tbody>
      </table>

</div>

</div>
@endsection
