
@extends('home')
<!-- categorie  -->
@section('content')
<div class="container">
    <div class="col-lg-4 py-3">
        <div class="widget-wrap2">
            <a class="btn btn-sm btn-primary" style="float: right;" href="{{ route('categories.create') }}">Ajouter categorie</a>
            <h3 style="text-align: center; color: black;">Categories et sous-categories </h3>
            <br>
            <ul class="categories">
                @foreach ($categories as $categorie)
                <li><a style="margin-right: 10px;">{{ $categorie->cat_nom}}
                        <a href="/categorie/{{$categorie->id}}/edit">
                            <div class="meta-item entry-author">
                                <div class="icon">
                                    <img src="../img/edit-icon.svg" style="width: 65%; height: 65%;"  >
                                </div>
                            </div>
                        </a>
                        <a href="/categorie/{{$categorie->id}}/show">
                            <div class="meta-item entry-author">
                                <div class="icon2">
                                    <img src="../img/eye.png" style="width: 65%; height: 65%;"  >
                                </div>
                            </div>
                        </a>
                    </a>
                    <br/>
                    @foreach ($sous as $sousCategories)
                        @if ($sousCategories->sous_cat_id == $categorie->id)
                            <a href="/souscategorie/{{$sousCategories->id}}/show"> {{  $sousCategories->sous_nom }} |</a>
                        @endif
                    @endforeach
                    <a style="float: right;" href="/souscategorie/{{$categorie->id}}/create">
                        <div class="meta-item entry-author">
                            <div class="icon">
                                <img src="../img/add-icon.svg"   style="width: 65%; height: 65%;"  >
                            </div>
                        </div>
                    </a>
                </li>
                <hr>
                 @endforeach
            </ul>
          </div>
    </div>

</div>

@endsection
