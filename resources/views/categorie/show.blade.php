@extends('home')

@section('content')

<table class="table">
    <h3 style="color:black;">Afficher des choses du catégorie << {{$categorie->cat_nom}} >></h3> <br>
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nom du chose</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($choses as $chose)
        @if ($chose->sousCategorie->sous_cat_id == $categorie->id)
        <tr>
            <th scope="row">{{$chose->id}}</th>
            <td>{{$chose->ch_nom}}</td>
            <td>
            <a href="/souscategorie/{{$chose->id}}/edit" class="btn btn-primary" >Modifier</a>
            <form action="/souscategorie/{{$chose->id}}/destroy" method="POST" style="display: inline">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Supprimer</button>
            </form>
            </td>
        </tr>
        @endif
      @endforeach
    </tbody>
</table>

@endsection
