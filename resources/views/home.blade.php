@extends('layout.layout')

@section('content')
<div class="content container pt-5" >
        <div class="container h-100">
          <div class="row align-items-center h-100">
            <div class="col-lg-6 wow fadeInUp">
              <div class="badge mb-2"><span class="icon mr-1"><span class="mai-globe"></span></span> 2020 Projet PWS</div>
              <h1 class="mb-4">Gestion des choses </h1>
              <p class="mb-4">Une place pour chaque chose et  <br>
                chaque chose à sa place.</p>
              <a href="/rechercher" class="btn btn-primary rounded-pill">Rechercher des choses </a>
            </div>
            </div>
          </div>
</div>
@endsection


