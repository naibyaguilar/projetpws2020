@extends('home')
<!-- LIEUX -->
@section('content')
<div class="widget-wrap">
    <h1 style="text-align: center; color: black;">ENREGISTRER UN LIEU</h1>
    <form action="/lieu" method="POST">
        @include('forms.form-lieu')
        <button type="submit" class="btn btn-primary">Ajouter lieu</button>
    </form>
</div>
@endsection
