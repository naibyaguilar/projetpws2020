@extends('home')
<!-- LIEU -->
@section('content')

<div class="page-section">
  <div class="container">
    <div class="widget-wrap">

        <a class="btn btn-sm btn-primary" style="float: right;" href="/lieu/create">Ajoute un lieu</a>
        <h3 class="widget-title" style="text-align: center; color: black;">LIEUX</h3>
        <br/>

          <div class="card-page">
            <div class="row row-beam-md">
              <div class="tab-content" id="myTabContent">
              </div>
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nom du lieu</th>
                    <th scope="col">Trouvé dans</th>
                    <th scope="col">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($lieux as $lieu)
                    <tr>
                    <th scope="col">{{$lieu->id}}</th>
                    <th scope="col">{{$lieu->li_nom}}</th>
                    <th scope="col">{{$lieu->parent['li_nom']}}</th>
                    <th>
                      <a href="/lieu/{{$lieu->id}}/edit" class="btn btn-info">Modifier</a>
                      <form action="/lieu/{{$lieu->id}}" method="POST" style="display: inline">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Supprimer</button>
                      </form>
                    </th>
                    </tr>
                  @endforeach
                </tbody>
              </table>

              </div>
          </div>
    </div>
  </div>
</div>
@endsection
