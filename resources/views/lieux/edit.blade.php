@extends('layout.layout')

@section('content')
<div class="widget-wrap">
<h1>Modification du lieu << {{$lieu->li_nom}} >></h1>
<form action="/lieu/{{$lieu->id}}" method="POST">
    @method('PATCH')
    @include('forms.form-lieu')
    <button type="submit" class="btn btn-primary">Sauvergarder les informations</button>
</form>
</div>
@endsection
