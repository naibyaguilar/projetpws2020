@csrf
<div class="form-group">
    <div class="row">
        <div class="col">
            <h3><label for="nom">Nom : </label></h3>
            <input name="cat_nom" type="text" class="form-control" placeholder="" value="{{ old('value') ?? $categorie->cat_nom}}">
        </div>
    </div>
</div>
