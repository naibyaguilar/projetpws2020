@csrf
<div class="form-group">
    <div class="row">
        <div class="col">
            <h3><label for="nom">Nom : {{ $sousCategorie->sous_nom }} </label></h3>
            <input name="sous_nom" type="text" class="form-control @error('sous_nom') is-invalid @enderror" placeholder="" value="{{ $sousCategorie->sous_nom}}">
            @error('sous_nom')
            <div class="invalid-feedback">
                {{ $errors->first('sous_nom') }}
            </div>
            @enderror
        </div>

    </div>
