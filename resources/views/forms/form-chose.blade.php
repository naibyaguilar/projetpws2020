@csrf
<div class="form-group">
    <div class="row">
        <div class="col">
            <label for="nom" style="color:black;">Nom du chose : </label>
            <input type="text" placeholder="Rentrez le chose ..." name="ch_nom" id="nom"
                 class="form-control @error('ch_nom') is-invalid @enderror" value={{$chose->ch_nom}}>
            @error('ch_nom')
            <div class="invalid-feedback">
                {{ $errors->first('ch_nom') }}
            </div>
            @enderror
        </div>
        <div class="col">
            <label for="select1" style="color:black;">Lieu</label>
            <select class="form-control @error('ch_li_id') is-invalid @enderror" id="select1" name="ch_li_id">
                <option selected>Sélectionner...</option>
                @foreach ($lieux as $item)
                    <option value="{{ $item->id }}">
                        {{$item->li_nom}}
                    </option>
                @endforeach
            </select>
            @error('ch_li_id')
              <div class="invalid-feedback">
                {{ $errors->first('li_dans_li') }}
              </div>
            @enderror
        </div>
    </div>
<br>
<div class="form-group">
    <div class="row">
        <div class="col">
            <label for="select2" style="color:black;"></label>Catégorie</label>
            <select class="form-control @error('cat') is-invalid @enderror" id="select2" name="cat">
                <option>Sélectionner...</option>
                @foreach ($categories as $item)
                    <option value="{{ $item->id }}" > <!--$chose->sousCategorie->categorie->id == $item->id ? 'selected' : '' }} -->
                        {{$item->cat_nom}}
                    </option>
                @endforeach
              </select>
              @error('cat')
              <div class="invalid-feedback">
                {{ $errors->first('cat') }}
                </div>
              @enderror
        </div>
        <div class="col">
            <label for="select3" style="color:black;">Sous-catégorie</label>
            <select class="form-control @error('ch_sous_id') is-invalid @enderror" id="select3" name="ch_sous_id">
              </select>
              @error('ch_sous_id')
              <div class="invalid-feedback">
                {{ $errors->first('ch_sous_id') }}
                </div>
              @enderror
        </div>
    </div>
</div>
<br>
<div class="form-group">
    <div class="row">
        <div class="col">
            <label for="select3" style="color:black;">Mots-Clé</label> <br>
            @foreach ($mot as $item)
            <div class="form-check form-check-inline">
                <input class="form-check-input" name="check_mot_cles[]" type="checkbox" id="motcle_mot_id" value="{{ $item->id }}">
                <label class="form-check-label" for="inlineCheckbox1">{{ $item->mot_nom }}</label>
            </div>
            @endforeach
            <a style="float: right;" href="/">
                <div class="meta-item entry-author">
                    <div class="icon">
                        <img src="../img/add-icon.svg"   style="width: 65%; height: 65%;"  >
                    </div>
                </div>
            </a>
         </div>
    </div>
</div>
