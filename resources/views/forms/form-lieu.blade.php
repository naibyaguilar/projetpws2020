@csrf
<div class="form-group">
    <div class="row">
        <div class="col">
            <label for="nom">Nom du lieu : </label>
            <input type="text" name="li_nom" placeholder="Rentrez un lieu ..."
                class="form-control @error('li_nom') is-invalid @enderror" id="" value="{{ old('value') ?? $lieu->li_nom}}">
            @error('li_nom')
            <div class="invalid-feedback">
                {{ $errors->first('li_nom') }}
            </div>
            @enderror
        </div>
        <div class="col">
            <label for="select1" style="color:black;">Lieu :</label>
            <select class="form-control @error('li_dans_id') is-invalid @enderror"
                 id="select1" name="li_dans_id">
                <option selected>Sélectionner...</option>
                @foreach ($lieux as $item)
                    @if ($lieu->id != $item->id)
                    <option value="{{ $item->id }}" {{ $lieu->li_dans_id == $item->id ? 'selected' : '' }}>
                        {{$item->li_nom}}
                    </option>
                    @endif
                @endforeach
              </select>
              @error('li_dans_li')
              <div class="invalid-feedback">
                {{ $errors->first('li_dans_li') }}
                </div>
              @enderror
        </div>
    </div>
</div>
