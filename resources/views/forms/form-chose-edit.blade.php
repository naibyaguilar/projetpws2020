@csrf
<div class="form-group">
    <div class="row">
        <div class="col">
            <label for="nom">Nom du chose {{$chose->ch_nom}}</label>
            <input type="text" placeholder="Rentrez le chose ..." name="ch_nom" id="nom" size="50"
                 class="form-control @error('ch_nom') is-invalid @enderror" value="{{ old('value') ?? $chose->ch_nom}}">
            @error('ch_nom')
            <div class="invalid-feedback">
                {{ $errors->first('ch_nom') }}
            </div>
            @enderror
        </div>
        <div class="col">
            <label for="select1" style="color:black;">Lieu</label>
            <select class="form-control @error('ch_li_id') is-invalid @enderror" id="select1" name="ch_li_id">
                <option selected>Default</option>
                @foreach ($lieux as $item)
                    <option value="{{ $item->id }}" {{ $chose->ch_li_id == $item->id ? 'selected' : '' }}>
                        {{$item->li_nom}}
                    </option>
                @endforeach
            </select>
            @error('ch_li_id')
              <div class="invalid-feedback">
                {{ $errors->first('li_dans_li') }}
              </div>
            @enderror
        </div>
    </div>
<br>
<div class="form-group">
    <div class="row">
        <div class="col">
            <label for="select2">Catégorie</label>
            <select class="form-control @error('cat') is-invalid @enderror" id="select2" name="cat">
                @foreach ($categories as $item)
                    <option value="{{ $item->id }}" {{$chose->sousCategorie->categorie->id == $item->id ? 'selected' : '' }}>
                        {{$item->cat_nom}}
                    </option>
                @endforeach
              </select>
              @error('cat')
              <div class="invalid-feedback">
                {{ $errors->first('cat') }}
                </div>
              @enderror
        </div>
        <div class="col">
            <label for="select3" style="color:black;">Sous-catégorie</label>
            <select class="form-control @error('ch_sous_id') is-invalid @enderror" id="select3" name="ch_sous_id">
              </select>
              @error('ch_sous_id')
              <div class="invalid-feedback">
                {{ $errors->first('ch_sous_id') }}
                </div>
              @enderror
        </div>
    </div>
</div>
<br>
