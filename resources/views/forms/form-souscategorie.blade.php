@csrf
<div class="form-group">
    <div class="row">
        <div class="col">
            <h3><label for="nom">Nom : </label></h3>
            <input name="sous_nom" type="text" class="form-control @error('sous_nom') is-invalid @enderror" placeholder="" value="{{old('value') ?? $SousCategorie->sous_nom}}">
            @error('sous_nom')
            <div class="invalid-feedback">
                {{ $errors->first('sous_nom') }}
            </div>
            @enderror
        </div>

    </div>
    <div class="row">
        <div class="col">
            <input name="sous_cat_id" hidden="true" type="number" class="form-control @error('sous_cat_id') is-invalid @enderror" value="{{old('value') ?? $categorie ->id }}">
            @error('sous_cat_id')
            <div class="invalid-feedback">
                {{ $errors->first('sous_cat_id') }}
            </div>
            @enderror
        </div>
    </div>

</div>

