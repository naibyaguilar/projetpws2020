@extends('home')
<!-- Rechercher  -->
@section('content')
<div class="container" >
    <div class="widget-wrap">
  <h3 class="widget-title">Recherche</h3>

  <form class="row justify-content-center" action="/recherche" method="POST" class="form-inline">
    @csrf
      <div class="form-group mx-sm-2 mb-2">
        <input type="search" placeholder="Chercher une chose ou lieu" name="nom" class="form-control" style="width: 500px;">
      </div>
      <button class="btn btn-outline-light mb-2" style="background-color: #2cced1" type="submit">Chercher</button>

  </form>
    <div class="tab-content" id="myTabContent">
        <div class="row justify-content-center">
          <div class="col-lg-10 my-3 wow fadeInUp">
              <div class="card-page">
                @if($message !="")
                    <div class="alert alert-info row justify-content-center" role="alert">
                        {{-- No hay resultados para la busqueda --}}
                        {{ $message }}
                    </div>
                @endif
                 <div class="row row-beam-md">
                     @if ($choses !="")
                     <table class="table">
                         <thead>
                             <tr>
                                 <th scope="col">#</th>
                                 <th scope="col">Nom</th>
                                 <th scope="col">Lieu</th>
                                 <th scope="col">Mots-clés</th>
                                 <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($choses as $item)
                                <tr>
                                    <th scope="row">{{$item->id}}</th>
                                    <td>{{$item->ch_nom}}</td>
                                    <td>{{$item->lieus->li_nom}}</td>
                                    <td>
                                       <div class="tag-clouds">
                                            <a class="tag-cloud-link">Laptop</a>
                                            <a class="tag-cloud-link">Negra</a>
                                        </div>
                                    </td>
                                    <td>
                                       <a href="/choses/{{$item->id}}/edit" class="btn btn-info">Modifier</a>
                                         <form action="/choses/{{$item->id}}" method="POST" style="display: inline">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">Supprimer</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                       @endif
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
