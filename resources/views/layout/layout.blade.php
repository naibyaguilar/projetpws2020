<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <!-- JS Bootstrap -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"
        integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous">
    </script>

    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="/css/card.css">

</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light navbar-floating" style="background-color:#3278fa">
        <div class="container">
            <a class="navbar-brand" href="/" style="color:#6610f2te">Projet PWS 2020</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarToggler">
              <ul class="navbar-nav ml-lg-5 mt-3 mt-lg-0">
                <li class="nav-item dropdown active">
                    <a class="nav-link" href="/choses/" aria-haspopup="true"
                    aria-expanded="false" style="color:white">Choses</a>
                </li>
                <li class="nav-item dropdown active">
                    <a class="nav-link" href="/lieu" aria-haspopup="true"
                    aria-expanded="false" style="color:white">Lieux</a>

                </li>
                <li class="nav-item dropdown active">
                    <a class="nav-link" href="/cat" aria-haspopup="true"
                    aria-expanded="false" style="color:white">Catégories</a>
                </li>

              </ul>
              <div class="ml-auto my-2 my-lg-0">
                <a href="/rechercher" class="btn btn-light rounded-pill" style="color: #383340">Rechercher</a>
              </div>

            </div>
          </div>
    </nav>
    <div class="page-hero-section bg-image">

    <div class="container" style="padding-top: 2%; padding-bottom: 20%">
        @yield('content')
    </div>

    </div>
    @yield('script')

</body>
</html>
